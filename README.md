# wire

Download entire projects from popular online repos

## Usage

```
usage: wire [-h] [--id ID] [--savepath SAVEPATH] [--savename SAVENAME]
                   [--w] [--service {openneuro}]

optional arguments:
  -h, --help            show this help message and exit
  --id ID               a dataset ID. Openneuro.org provides these in the form
                        of ds###### (e.g. ds002190)
  --savepath SAVEPATH   the output directory where you would like to download
                        the dataset (default is the current working directory)
  --savename SAVENAME   savename is an optional name to give the downloaded
                        dataset folder. The default is the dataset id.
  --w                   re-sync existing dataset with the same ID if it
                        already exists in the savepath
  --service {openneuro}
                        set the service provider that you want to download
                        data from
```
