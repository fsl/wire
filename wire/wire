#!/usr/bin/env fslpython
"""
usage: wire [-h] [--savepath SAVEPATH] [--savename SAVENAME] [--w] id

download a dataset from popular online repos

positional arguments:
  id                   a dataset ID. Openneuro.org provides these in the form
                       of ds###### (e.g. ds002285)

optional arguments:
  -h, --help           show this help message and exit
  --savepath SAVEPATH  the output directory where you would like to download
                       the dataset (default is the current working directory)
  --savename SAVENAME  savename is an optional name to give the downloaded
                       dataset folder. The default is the dataset id.
  --w                  re-sync existing dataset with the same ID if it
                       already exists in the savepath
"""
import argparse
from os.path import abspath, join, exists
from os import getcwd, getenv
import sys
from subprocess import run

pwd = abspath(getcwd())

# describe the commandline tool
parser = argparse.ArgumentParser(
    description="download a dataset from popular online repos"
)
# id must be a string and is not optional
parser.add_argument(
    '--id',
    type=str,
    help="a dataset ID. Openneuro.org provides these in the form of ds###### (e.g. ds002190)",
    default=''
)
# savepath must be a string and is optional. the default is the current working dir
parser.add_argument(
    '--savepath',
    type=str,
    help="the output directory where you would like to download the dataset (default is the current working directory)",
    default=pwd,
)
# savename is an optional name to give the downloaded dataset folder. The default is the dataset id.
parser.add_argument(
    '--savename',
    type=str,
    help="savename is an optional name to give the downloaded dataset folder. The default is the dataset id.",
    default=None,
)
# overwrite is a boolean flag and is optional. The default is false (no overwriting)
parser.add_argument(
    '--w',
    help="re-sync existing dataset with the same ID if it already exists in the savepath",
    default=False,
    action='store_true'
)
# specify an open data service to use
parser.add_argument(
    '--service',
    help="set the service provider that you want to download data from",
    choices=['openneuro'],
    default='openneuro'
)

def main():
    args = parser.parse_args()
    savename = args.savename
    if savename is None:
        savename = args.id

    savepath = abspath(
        join(
            args.savepath, savename
        )
    )
    overwrite = args.w
    if exists(savepath):
        if not overwrite:
            print("ERROR: savepath {} already exists, not overwriting. Try the --w flag if you wish to re-sync the data with what appears on openneuro".format(savepath))
            sys.exit(1)
    if args.id[:2] != "ds":
        print("ERROR: an openneuro dataset id must start with 'ds'. Please check your dataset id")
        sys.exit(1)

    FSLDIR = getenv('FSLDIR') # returns none if not found
    if FSLDIR is None:
        print("ERROR: Unable to find FSLDIR evironment variable")
        sys.exit(1)

    awsbin = join(FSLDIR, "fslpython", "envs", "fslpython", "bin", "aws")

    # use subprocess.run to call the aws command to sync from the aws bucket (download the dataset)
    run(
        [
            awsbin,
            "s3",
            "sync",
            "--no-sign-request",
            "s3://openneuro.org/{}".format(
                args.id
            ),
            savepath
        ]
    )


if __name__ == "__main__":
    sys.exit(main())
